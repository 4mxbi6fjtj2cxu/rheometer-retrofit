# Multiwindow example
# https://gist.github.com/MalloyDelacroix/2c509d6bcad35c7e35b1851dfc32d161

import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


class Widget0(QWidget):

    switch_window = pyqtSignal()

    def __init__(self, parent=None):
        #super(Widget0, self).__init__(parent)
        #super().__init__()
        QWidget.__init__(self, parent)
        self.message = QLabel('I am Win0')
        self.button = QPushButton(text="Open Win1")
        self.setGeometry(100,100,400,300)
        self.setWindowTitle("Window0")
        layout = QVBoxLayout(self)
        layout.addWidget(self.message)
        layout.addWidget(self.button)
        self.setLayout(layout)
        self.button.clicked.connect(self.toggle)

    def toggle(self, other):
        self.switch_window.emit()


class Widget1(QWidget):

    switch_window = pyqtSignal()

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.message = QLabel('I am Win1')
        self.button = QPushButton(text="Open Win2")
        self.setGeometry(100,100,400,300)
        self.setWindowTitle("Window1")
        layout = QVBoxLayout(self)
        layout.addWidget(self.message)
        layout.addWidget(self.button)
        self.setLayout(layout)
        self.button.clicked.connect(self.toggle)

    def toggle(self, other):
        self.switch_window.emit()


class Widget2(QWidget):

    switch_window = pyqtSignal()

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.message = QLabel('I am Win2')
        self.button = QPushButton(text="Open Win0")
        self.setGeometry(100,100,400,300)
        self.setWindowTitle("Window2")
        layout = QVBoxLayout(self)
        layout.addWidget(self.message)
        layout.addWidget(self.button)
        self.setLayout(layout)
        self.button.clicked.connect(self.toggle)

    def toggle(self, other):
        self.switch_window.emit()


class Flow_control:

    def __init__(self):
        self.w0 = Widget0()
        self.w1 = Widget1()
        self.w2 = Widget2()

    def show_win0(self):
        self.w0.switch_window.connect(self.show_win1)
        self.w1.hide()
        self.w2.hide()
        self.w0.show()

    def show_win1(self):
        self.w1.switch_window.connect(self.show_win2)
        self.w0.hide()
        self.w2.hide()
        self.w1.show()

    def show_win2(self):
        self.w2.switch_window.connect(self.show_win0)
        self.w0.hide()
        self.w1.hide()
        self.w2.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    fc = Flow_control()
    fc.show_win0()
    sys.exit(app.exec())
