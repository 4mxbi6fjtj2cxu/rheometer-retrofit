int incomingByte = 0;   // for incoming serial data
int ledPin = 13;

void setup() {
  Serial.begin(57600);  // initialize the serial communication 57600 baudrate
  pinMode(ledPin, OUTPUT);
  while (Serial.read() != 126) {} // wait for ~ character to initialize readings
}

void loop() {
   if (digitalRead(ledPin) == "o") {
	   digitalWrite(ledPin, LOW);
	   }
   if (digitalRead(ledPin) == "i") {
       digitalWrite(ledPin, HIGH);
       }
}
