int rly0 = 3;     // Relay 0 - Motor
int rly1 = 4;     // Relay 1 - Apertura y cierre de placas
int rly2 = 5;     // Relay 2 - Sujección de disco
int rly3 = 6;     // Relay 3 - Fuente de poder
int sw0 = 2;      // Switch 0 - (switch magnético en instrumento)
int led0 = 13;    // LED 0 - Indicador de estado de SW0
char input = ' '; // Instrucción enviada desde Core.py

void setup() {
  pinMode(rly0, OUTPUT);
  pinMode(rly1, OUTPUT);
  pinMode(rly2, OUTPUT);
  pinMode(rly3, OUTPUT);
  pinMode(sw0, INPUT_PULLUP);
  pinMode(led0, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(sw0)==LOW)
    {  
     int sensorValue = analogRead(A0);
     Serial.println(sensorValue, DEC);
     digitalWrite(led0, HIGH);
    }
  if (digitalRead(sw0)==HIGH)
    {
     digitalWrite(led0, LOW); 
    }

  if (Serial.available() > 0)
    {
     input = Serial.read();
    }

  switch (input){
    case 'A':
      digitalWrite(rly0, HIGH);
      input = ' ';
      break;
    case 'B':
      digitalWrite(rly0, LOW);
      input = ' ';
      break;
    case 'C':
      digitalWrite(rly1, HIGH);
      input = ' ';
      break;
    case 'D':
      digitalWrite(rly1, LOW);
      input = ' ';
      break;
    case 'E':
      digitalWrite(rly2, HIGH);
      input = ' ';
      break;
    case 'F':
      digitalWrite(rly2, LOW);
      input = ' ';
      break;
    case 'G':
      digitalWrite(rly3, HIGH);
      input = ' ';
      break;
    case 'H':
      digitalWrite(rly3, LOW);
      input = ' ';
      break;
  }
}
