# -*- coding: utf-8 -*-

import sys # app = QApplication(sys.argv) puede no ser necesario porque no hay argumentos
import serial # pyserial maneja los eventos del serial (USB)
import os # devs = os.listdir('/dev/') y para cerrar aplicación e informar pid - print os.getpid() os.kill(os.getpid(), 9)
import re #para encontrar serial devices en /dev/*ACM*
import threading #run y start???

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from matplotlib.backends.backend_qt4agg import \
    FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import \
    NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.figure import Figure
import numpy as np # manejo de la cadena de valores para crear un arreglo y generar archivo csv


class ReadArduino(threading.Thread):
    """Esta clase lee constantemente la salida del arduino y hace disponible
    el ultimo valor. 
    
    Los metodos controlan el estado de las salidas 2-8"""
    def __init__(self):
        self.active = True

        # Obtiene el nombre de los dipositivos arduino
        devs = os.listdir('/dev/')
        devs = filter(self.name, devs)

        # Inicializa serial
        self.arduinos = []
        for dev in devs:
            try:
                self.ser = serial.Serial('/dev/' + dev, 9600)
                self.arduinos.append(dev)
            except:
                print '/dev/' + dev + ' no esta disponible'
            else:
                break

        threading.Thread.__init__(self)

    def name(self, dev):
        return re.match(r'.*ACM', dev)

    def run(self):
        while self.active:
             self.data = self.ser.readline()

    def MTR_EN(self, estado):
        comando = 'A' if estado else 'B'
        self.ser.write(comando)

    def MTR_DIR(self, estado):
        comando = 'C' if estado else 'D'
        self.ser.write(comando)

    def AUX1(self, estado):
        comando = 'E' if estado else 'F'
        self.ser.write(comando)

    def AUX2(self, estado):
        comando = 'G' if estado else 'H'
        self.ser.write(comando)

    def DP_UD(self, estado):
        comando = 'K' if estado else 'L'
        self.ser.write(comando)

    def DP_INC(self, estado):
        comando = 'M' if estado else 'N'
        self.ser.write(comando)

    def DP_CS(self, estado):
        comando = 'I' if estado else 'J'
        self.ser.write(comando)

    def stop(self):
        self.active = False
        print os.getpid()
        os.kill(os.getpid(), 9)


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.createGUI()

    def createGUI(self):
        #Crea y configura todos los elementos de la interfaz
        self.arduino = ReadArduino()
        
        if len(self.arduino.arduinos) < 1:
										mensaje = QMessageBox(self)
										mensaje.setText('No hay interfaz "/dev/ttyACM*" disponible. Desconecte, reconecte y reinicie el programa')
										mensaje.setWindowTitle('Hardware no disponible')
										mensaje.setIcon(QMessageBox.Critical)
										mensaje.exec_()
										exit()
        self.arduino.start()

        # Generacion de elementos principales de la grafica
        self.dpi = 100
        self.fig = Figure((6.0, 5.0), dpi=self.dpi)
        self.canvas = FigureCanvas(self.fig)
        #self.canvas.setParent(self.frame)

        # Configuracion de la grafica
        self.axes = self.fig.add_subplot(111)
        self.axes.set_axis_bgcolor('black')
        self.axes.grid(True, color='gray')

        # Declaracion de interfaz
        self.pause = QPushButton('Pausar')
        self.pause.setDisabled(True)
        self.pause.clicked.connect(self.doPause)

        self.comenzar = QPushButton('Comenzar')
        self.comenzar.setCheckable(True)
        self.comenzar.toggled.connect(self.togglePrueba)

        self.guardar = QPushButton('Guardar')
        self.guardar.clicked.connect(self.doGuardar)

        self.nombreL = QLabel('Nombre de la prueba')
        self.nombre = QLineEdit()

        self.tiempoPrueba = QSpinBox()
        self.tiempoPrueba.setMinimum(1)
        self.tiempoPrueba.setDisabled(True)

        self.grupoTiempo = QGroupBox('Manejo de tiempo')
        self.grupoSignal = QGroupBox(u'Señales')
        self.politicaTiempo = QButtonGroup()
        self.tiempoFijo = QRadioButton('Tiempo fijo')
        self.tiempoIlimitado = QRadioButton('Tiempo ilimitado')
        self.tiempoIlimitado.setChecked(True)
        self.tiempoIlimitado.toggled.connect(self.cambiaPolitica)

        self.MTR_EN = QPushButton('MTR_EN')
        self.MTR_DIR = QPushButton('MTR_DIR v')
        self.AUX1 = QPushButton('AUX1')
        self.AUX2 = QPushButton('AUX2')
        self.DP_CS = QPushButton('DP_CS')
        self.DP_UD = QPushButton('DP_UD')
        self.DP_INC = QPushButton('DP_INC')
        self.MTR_EN.setCheckable(True)
        self.MTR_DIR.setCheckable(True)
        self.AUX1.setCheckable(True)
        self.AUX2.setCheckable(True)
        self.DP_CS.setCheckable(True)
        self.DP_UD.setCheckable(True)
        self.DP_INC.setCheckable(False)
        self.MTR_EN.toggled.connect(self.arduino.MTR_EN)
        self.MTR_DIR.toggled.connect(self.arduino.MTR_DIR)
        self.AUX1.toggled.connect(self.arduino.AUX1)
        self.AUX2.toggled.connect(self.arduino.AUX2)
        self.DP_CS.toggled.connect(self.arduino.DP_CS)
        self.DP_UD.toggled.connect(self.arduino.DP_UD)
        self.DP_INC.toggled.connect(self.arduino.DP_INC)

        # Configuracion de la interfaz
        gt = QGridLayout()
        gt.addWidget(self.tiempoIlimitado, 0, 0)
        gt.addWidget(self.tiempoFijo, 1, 0)
        gt.addWidget(self.tiempoPrueba, 1, 1)
        gt.addWidget(self.pause, 0, 2, 2, 1)
        self.grupoTiempo.setLayout(gt)

        gs = QGridLayout()
        gs.addWidget(self.MTR_EN, 2, 2)
        gs.addWidget(self.MTR_DIR, 2, 3)
        gs.addWidget(self.AUX1, 3, 2)
        gs.addWidget(self.AUX2, 3, 3)
        gs.addWidget(self.DP_CS, 2, 4)
        gs.addWidget(self.DP_UD, 3, 4)
        gs.addWidget(self.DP_INC, 4, 4)
        
        self.grupoSignal.setLayout(gs)

        gp = QGridLayout()
        gp.addWidget(self.nombreL, 1, 0)
        gp.addWidget(self.nombre, 1, 1)
        gp.addWidget(self.comenzar, 1, 2)
        gp.addWidget(self.guardar, 1, 3)
        gp.addWidget(self.grupoTiempo, 2, 0, 1, 2)
        gp.addWidget(self.grupoSignal, 2, 2, 1, 2)
        gp.addWidget(self.canvas, 0, 0, 1, 4)

        self.frame = QWidget()
        self.frame.setLayout(gp)
        self.setCentralWidget(self.frame)

        self.data = []
        self.line = self.axes.plot(
            self.data,
            linewidth=1,
            color=(1, 1, 0),
            )[0]
        

        # Manejo de tiempo
        self.tiempoTranscurrido = 0
        self.contadorActializa = QTimer()
        self.contadorActializa.timeout.connect(self.refresh);
        self.lecturasXSegundo = 10
        self.contadorActializa.setInterval(1000 / self.lecturasXSegundo)

        self.contadorPrincipal = QTimer()
        self.contadorPrincipal.timeout.connect(self.comenzar.toggle)

        self.draw_chart()

    def detenerPrueba(self):
        self.contadorActializa.stop()
        self.contadorPrincipal.stop()
        self.comenzar.setText('Comenzar')
        self.pause.setText('Pausar')
        self.pause.setDisabled(True)

    def comenzarPrueba(self):
        self.data = []
        self.time = []
        self.tiempoTranscurrido = 0
        self.draw_chart()
        if self.tiempoFijo.isChecked():
            self.contadorPrincipal.setInterval(
                self.tiempoPrueba.value() * 1000)
        else:
            self.contadorPrincipal.setInterval(525600000)  # Un año
        self.contadorActializa.start()
        self.contadorPrincipal.start()
        self.comenzar.setText('Detener')
        self.pause.setDisabled(False)
        self.arduino.MTR_DIR('C')
        self.arduino.MTR_EN('A')
        


    def togglePrueba(self):
        """Metodo para comenzar y detener la prueba actual"""
        if self.comenzar.isChecked():
            self.comenzarPrueba()
        else:
            self.detenerPrueba()

    def cambiaPolitica(self):
        """Habilita la interfaz para manejo manual del tiempo"""
        if self.tiempoIlimitado.isChecked():
            self.tiempoPrueba.setDisabled(True)
        else:
            self.tiempoPrueba.setEnabled(True)

    def doGuardar(self):
        """Verifica la existencia de datos y los guarda en png, cvs y pdf"""
        if self.nombre.text() == '':
            mensaje = QMessageBox(self)
            mensaje.setText('Ingrese un nombre para la prueba')
            mensaje.setWindowTitle('Error al guardar')
            mensaje.setIcon(QMessageBox.Critical)
            mensaje.exec_()
        elif len(self.data) == 0:
            mensaje = QMessageBox(self)
            mensaje.setText('No hay datos para guardar')
            mensaje.setWindowTitle('Error al guardar')
            mensaje.setIcon(QMessageBox.Critical)
            mensaje.exec_()
        else:
            nombre = str(self.nombre.text().toUtf8())
            archivo = open(nombre + '.csv', 'w')
            for n in range(len(self.data)):
                archivo.write(str(self.time[n]) + ',' + str(self.data[n]) + '\n')
            archivo.close()

            pdf = PdfPages(nombre + '.pdf')
            self.fig.savefig(pdf, format='pdf')
            pdf.close()

            self.canvas.print_figure(nombre + '.png', dpi=self.dpi)

            mensaje = QMessageBox(self)
            mensaje.setText('La prueba ha sido guardada correctamente')
            mensaje.setWindowTitle('Guardado con exito')
            mensaje.setIcon(QMessageBox.Information)
            mensaje.exec_()

    def doPause(self):
        """Maneja las pausas de la aplicación"""
        if self.contadorActializa.isActive():
            self.contadorActializa.stop()
            self.contadorPrincipal.stop()
            self.pause.setText('Reanudar')
        else:
            self.contadorActializa.start()
            # Recalcula el tiempo restante de la prueba
            self.contadorPrincipal.setInterval(
                self.tiempoPrueba.value() * 1000 - self.tiempoTransscurrido)
            self.contadorPrincipal.start()
            self.pause.setText('Pausar')

    def refresh(self):
        """Agrega un dato al conjunto y regenera la gráfica"""
        self.tiempoTranscurrido += 1000 / self.lecturasXSegundo
        self.time.append(self.tiempoTranscurrido)
        self.data.append(int(self.arduino.data))
        self.draw_chart()

    def draw_chart(self):
        """Regenera la grafica"""
        """/ 110.7421875"""
        np.array(map(float, self.data)) 
        count = round(len(self.data) * 1.0 / self.lecturasXSegundo, 3)
        xmax = count if count > 2 else 2

        self.axes.set_xbound(lower=0, upper=xmax)
        self.axes.set_ybound(lower=0, upper=1024)

        self.line.set_xdata(np.arange(0, count, 1.0 / self.lecturasXSegundo))
        self.line.set_ydata(np.array(self.data,float))

        self.canvas.draw()

    def closeEvent(self, event):
        self.arduino.stop()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()

# vim: ts=4 et sw=4 st=4
