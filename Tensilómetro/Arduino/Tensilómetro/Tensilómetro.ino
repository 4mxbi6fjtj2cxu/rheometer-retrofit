/*
Este diseño no utiliza los puertos 0 y 1 puesto que tienen la función de comunicación serial bidireccional.
pin0 = Rx.
pin1 = Tx.

Cuando la funcion "Serial.available();" es mayor a 0 hay un caracter en el buffer (64 bytes) esperando a ser transmitido. La función "Serial.read();" adquiere el caracter enviado por terminal serial al Arduino y entra en un switch case para decidir qué puertos operar con el dato y posteriormente reinicializa la variable input (' ').

La función "Serial.println();" escribe en la terminal serial en formato decimal (0-1024) el valor obtenido en el puerto A0 del Arduino. 

*/

// Variables globales

const int MTR_EN = 5;	// Conecta el motoreductor a la línea.
const int MTR_DIR = 6;	// Cambia la polaridad del capacitor de arranque del motoreductor, cambiando el sentido del giro.
const int AUX1 = 7; 	// Relay auxiliar 1 (10A)
const int AUX2 = 8;		// Relay auxiliar 2 (10A)
char input = ' '; 		// Señal proveniente de la interfaz qt-python. Controla botones. 
int sensorValue = 0;
bool S_MTREN = false;
bool S_MTRDIR = false;
bool S_AUX1 = false;
bool S_AUX2 = false;

bool DATATRANS = false;

void setup() 
{
	pinMode(MTR_EN, OUTPUT);
	pinMode(MTR_DIR, OUTPUT);
	pinMode(AUX1, OUTPUT);
	pinMode(AUX2, OUTPUT);
	digitalWrite(MTR_EN, LOW);
	digitalWrite(MTR_DIR, LOW);
	digitalWrite(AUX1, LOW);
	digitalWrite(AUX2, LOW);
	Serial.begin(9600);
 }

void loop() 
{
if (Serial.available() > 0) 
	{
	input = Serial.read();
	switch (input)
		{
		case 'A':
      		Serial.print("Received: ");
		Serial.println(input);
      		Serial.print("S_MTREN = ");
		S_MTREN = !S_MTREN;
		Serial.println(S_MTREN);
		if (S_MTREN == true)
			{
			digitalWrite(MTR_EN, HIGH);
			}
		else digitalWrite(MTR_EN, LOW);
		input = ' ';
		break;

		case 'B':
      		Serial.print("Received: ");
		Serial.println(input);
      		Serial.print("S_MTRDIR = ");
		S_MTRDIR = !S_MTRDIR;
		Serial.println(S_MTRDIR);
		if (S_MTRDIR == true)
			{
			digitalWrite(MTR_DIR, HIGH);
			Serial.println("up");
			}
		else {
			digitalWrite(MTR_DIR, LOW);
			Serial.println("down");
			}
		input = ' ';
		break;

		case 'C':
      		Serial.print("Received: ");
		Serial.println(input);
      		Serial.print("S_AUX1 = ");
		S_AUX1 = !S_AUX1;
		Serial.println(S_AUX1);
		if (S_AUX1 == true)
			{
			digitalWrite(AUX1, HIGH);
			Serial.println("up");
			}
		else {
			digitalWrite(AUX1, LOW);
			Serial.println("down");
			}
		input = ' ';
		break;
		
		case 'D':
      		Serial.print("Received: ");
		Serial.println(input);
      		Serial.print("S_AUX2 = ");
		S_AUX2 = !S_AUX2;
		Serial.println(S_AUX2);
		if (S_AUX2 == true)
			{
			digitalWrite(AUX2, HIGH);
			Serial.println("up");
			}
		else {
			digitalWrite(AUX2, LOW);
			Serial.println("down");
			}
		input = ' ';
		break;
		
		case '~':
		if (DATATRANS == false) 
			{
			DATATRANS = true;
			Serial.println("Begin Tx");
			}
		else 
			{
			DATATRANS = false;
			Serial.println("End Tx");
			}
		input = ' ';
		break;
		
		default:
		Serial.print("Received: ");
		Serial.println(input);
		Serial.println("Transmit Error (PC > Arduino)");
		break;
		}
	}
Serial.flush();					// Limpia el buffer
delay(10);

if (DATATRANS == true)
	{
	sensorValue = analogRead(A0);
	sensorValue = sensorValue - 10;	// cifra para calibración
	Serial.println(sensorValue, DEC);
	}

}