#!/usr/bin/python3
import serial
import time
import sys
#import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.animation as animation

try: 
	arduino = serial.Serial('/dev/ttyACM0', 115200)
	print('Starting connection to arduino. Please wait...')
	time.sleep(1.5)
	arduino.write(b'1')
	#print(str(arduino.readline()))
except (serial.SerialException):
	print('Arduino not connected!')
	sys.exit(1)

load_cell = []
time_stream = []
temp_stream = []

try:
	while True:
		in0 = str(arduino.readline())
		in0 = in0.replace('b\'', '')
		in0 = in0.replace('\\r\\n\'', '')
		try:
			in0 = in0.split(' ')
			mag_sw = int(in0[0])
			time_stream.append(time.time())
			load_cell.append(float(in0[1]))
			temp = float(in0[2])
			temp_stream.append(temp)
			print(time.time(),mag_sw, float(in0[1]), temp)
		except (ValueError):
			pass
		except (IndexError):
			pass

		
except (KeyboardInterrupt):
	print('Cleaning up and exiting')
	arduino.write(b'0')		# Stops arduino serial transmission
	
	plt.style.use('ggplot')
	plt.figure(1)			# set a figure number to insert plots into
	plt.subplot(211)		# 211 - numrows, numcols, figure 
	plt.plot(time_stream, load_cell, 'b-', linewidth=1)
	
	plt.subplot(212)
	plt.plot(time_stream, temp_stream, 'r-', linewidth=0.5)
	
	plt.show()
	plt.close()
	sys.exit(0)
	




'''
sida = input('Write something to start > ')
sida = bytes(sida, encoding='UTF-8')
arduino0.write(sida)

from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

pool = ThreadPool(4)
pool.map(target, input)
pool.close()
pool.join()
'''
