#include <Q2HX711.h>
#include "max6675.h"

const byte hx711_data_pin = 8;
const byte hx711_clock_pin = 9;
int mag_sw = 7;
int thermo0DO = 5;
int thermo0CS = 4;
int thermo0CLK = 6;

Q2HX711 hx711(hx711_data_pin, hx711_clock_pin);
MAX6675 thermocouple0(thermo0CLK, thermo0CS, thermo0DO);
MAX6675 thermocouple1(thermo0CLK, A0, thermo0DO);

float loadcell_val = 0.0;
float thermo0_val = 0.0;
float thermo1_val = 0.0;

boolean sw_val = false;
unsigned long time_in = millis();
String s_output = "";
boolean send_data_FLAG = false;
int input;
boolean emergency_stop = false;

void setup() {
  Serial.begin(115200);
  pinMode(mag_sw, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(11, OUTPUT);
  digitalWrite(11, HIGH);
  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);
  pinMode(3, OUTPUT);
  digitalWrite(3, HIGH);
  pinMode(A1, OUTPUT);
  digitalWrite(A1, HIGH);
  delay(1000);
  thermo0_val = thermocouple0.readCelsius();
  thermo1_val = thermocouple1.readCelsius();
  while (Serial.available() == 0) {}   // wait for python to intiate the transfer
}

void send_data() {
  
  loadcell_val = hx711.read();
  sw_val = digitalRead(mag_sw);
  
  if ((millis() - time_in) >= 300 ) {
    thermo0_val = thermocouple0.readCelsius();
    thermo1_val = thermocouple1.readCelsius();
    time_in = millis();
  }

  Serial.print(!sw_val);
  Serial.print(' ');
  Serial.print(loadcell_val);
  Serial.print(' ');
  Serial.print(thermo0_val);
  Serial.print(' ');
  Serial.println(thermo1_val);
  }


void serialEvent() { 
  char input = (char)Serial.read();
  switch (input) {
    case '0':
            send_data_FLAG = false;
            break;
    case '1':
            send_data_FLAG = true;
            break;
    case '2':
            digitalWrite(10, HIGH);
            break;
    case '3':
            digitalWrite(10, LOW);
            break;    
    case '4':
            digitalWrite(11, HIGH);
            break;    
    case '5':
            digitalWrite(11, LOW);
            break;    
    case '6':
            digitalWrite(12, HIGH);
            break;    
    case '7':
            digitalWrite(12, LOW);
            break;    
    case '8':
            digitalWrite(3, HIGH);
            break;    
    case '9':
            digitalWrite(3, LOW);
            break;    
    default:
            Serial.print("Invalid data received: ");
            Serial.println(input);
            break;
    }
}

void loop() {
    if (send_data_FLAG == true) { send_data(); }   
}
