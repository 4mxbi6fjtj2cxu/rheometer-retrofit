#!/usr/bin/python3
import serial
import time
import sys
#import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.animation as animation

try: 
	arduino = serial.Serial('/dev/ttyACM0', 57600)
	print('Starting connection to arduino. Please wait...')
	time.sleep(1.5)
	arduino.write(b'1')
	#print(str(arduino.readline()))
except (serial.SerialException):
	print('Arduino not connected!')
	sys.exit(1)

load_cell = []
time_stream = []

try:
	while True:
		in0 = str(arduino.readline())
		in0 = in0.replace('b\'', '')
		in0 = in0.replace('\\r\\n\'', '')
		try:
			in0 = in0.split(' ')
			mag_sw = int(in0[0])
			time_stream.append(time.time())
			load_cell.append(float(in0[1]))
			temp = float(in0[2])
			print(time.time(),mag_sw, temp, float(in0[1]))
		except (ValueError):
			pass
		except (IndexError):
			pass

		
except (KeyboardInterrupt):
	print('Cleaning up and exiting')
	arduino.write(b'0')
	
	plt.plot(time_stream,load_cell)
	plt.show()
	sys.exit(0)
	




'''
#sida = input('Write something to start > ')
#sida = bytes(sida, encoding='UTF-8')
#arduino0.write(sida)
'''

'''
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool

pool = ThreadPool(4)
pool.map(target, input)
pool.close()
pool.join()
'''
