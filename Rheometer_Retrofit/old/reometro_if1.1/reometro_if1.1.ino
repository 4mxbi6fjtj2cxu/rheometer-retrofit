#include <Q2HX711.h>
#include "max6675.h"

const byte hx711_data_pin = 8;
const byte hx711_clock_pin = 9;
int mag_sw = 7;
int thermoDO = 5;
int thermoCS = 4;
int thermoCLK = 6;

Q2HX711 hx711(hx711_data_pin, hx711_clock_pin);
MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);

float loadcell_val = 0.0;
float thermo0_val = 0.0;
boolean sw_val = false;
unsigned long time_in = millis();
String s_output = "";
boolean send_data_FLAG = false;
int input;

void setup() {
  Serial.begin(115200);
  pinMode(mag_sw, INPUT_PULLUP);
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(11, OUTPUT);
  digitalWrite(11, HIGH);
  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);
  delay(1000);
  thermo0_val = thermocouple.readCelsius();
  while (Serial.available() == 0) {}   // wait for python to intiate the transfer
  Serial.println("Begin data transmission: ");  // send 1st msg
}

void send_data() {
  loadcell_val = hx711.read()/8.75 - 981620;
  sw_val = digitalRead(mag_sw);
  
  if ((millis() - time_in) >= 300 ) {
    thermo0_val = thermocouple.readCelsius();
    time_in = millis();
  }
  Serial.print(!sw_val);
  Serial.print(' ');
  Serial.print(loadcell_val);
  Serial.print(' ');
  Serial.println(thermo0_val);
  }

void loop() {
    if (send_data_FLAG == true) { send_data(); }
    
}


void serialEvent() { 
  char input = (char)Serial.read();
  switch (input) {
    case '0':
            send_data_FLAG = false;
            break;
    case '1':
            send_data_FLAG = true;
            break;
    case '2':
            digitalWrite(10, HIGH);
            break;
    case '3':
            digitalWrite(10, LOW);
            break;    
    case '4':
            digitalWrite(11, HIGH);
            break;    
    case '5':
            digitalWrite(11, LOW);
            break;    
    case '6':
            digitalWrite(11, HIGH);
            break;    
    case '7':
            digitalWrite(11, LOW);
            break;    
    case '8':
            digitalWrite(11, HIGH);
            break;    
    case '9':
            digitalWrite(11, LOW);
            break;    
    default:
            Serial.print("Invalid data received: ");
            Serial.println(input);
            break;
    }
}
