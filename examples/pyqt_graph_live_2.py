#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import collections, random

win = pg.GraphicsWindow()
win.setWindowTitle('pyqtgraph example: Scrolling Plots')

# 2) Allow data to accumulate. In these examples, the array doubles in length whenever it is full.

p1 = win.addPlot()
#win.nextRow()

# Use automatic downsampling and clipping to reduce the drawing load
p1.setDownsampling(mode='peak')
p1.setClipToView(True)
#p1.setRange(xRange=[-100,0])
#p1.setLimits(xMax=0)

x = collections.deque()
y = collections.deque()

x.append(0)
y.append(0)

def update():
    x.append(x[-1]+1)
    y.append(random.randint(100,1000))
    p1.plot(x, y, pen='g')
	

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(100)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
         QtGui.QApplication.instance().exec_()
