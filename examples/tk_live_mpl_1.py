from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as Tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import collections
import random
import time
#---------End of imports

fig = plt.Figure()
#x = np.arange(0, 2*np.pi, 0.01)        # x-array
x = collections.deque([0])
y = collections.deque([0])

def animate(i):
    #line.set_xdata(time.time())  # update the data
    #line.set_ydata(random.randint(0,101))
    x.append(time.time())
    y.append(random.randint(0,101))
    #return line,

root = Tk.Tk()
label = Tk.Label(root,text="SHM Simulation").grid(column=0, row=0)
canvas = FigureCanvasTkAgg(fig, master=root)
canvas.get_tk_widget().grid(column=0,row=1)

ax = fig.add_subplot(111)
#line, = ax.plot(x, y)
ax.plot(x,y)
ani = animation.FuncAnimation(fig, animate, np.arange(1, 200), interval=10, blit=False)

Tk.mainloop()
