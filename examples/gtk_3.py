import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title='name o\' window')

        self.box0 = Gtk.Box(spacing=5)
        self.add(self.box0)


        self.button0 = Gtk.Button(label='button 0')
        self.button0.connect('clicked', self.button_clicked, 0)
        self.box0.pack_start(self.button0, True, True, 0)        
        
        self.button1 = Gtk.Button(label='button 1')
        self.button1.connect('clicked', self.button_clicked, 1)
        self.box0.pack_start(self.button1, True, True, 0)        
        
        
        
    def button_clicked(self, widget, data):
        if data == 0:
            print('button0 pressed', data)
        elif data == 1:
            print('button1 pressed', data)
        else:
            print('error')
        
if __name__ == '__main__':
    win0 = MyWindow()
    win0.connect('delete-event', Gtk.main_quit)
    win0.show_all()
    Gtk.main()
    print('program returned ok')
