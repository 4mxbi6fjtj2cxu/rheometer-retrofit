import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title='name o\' window')

        self.button0 = Gtk.Button(label='click me')
        self.button0.connect('clicked', self.button0_clicked, 1)
        self.add(self.button0)
        print(dir(self.button0.props))        
        
    def button0_clicked(self, widget, data):
        print('button0 pressed', data)

if __name__ == '__main__':
    win0 = MyWindow()
    win0.connect('delete-event', Gtk.main_quit)
    win0.show_all()
    Gtk.main()
    print('program returned ok')
