#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from pyqtgraph.ptime import time
import serial, random, collections, time

app = QtGui.QApplication([])

p = pg.plot()
curve = p.plot(autoDownsample=True, clipToView=True, antialias=True, pen='g')


data = collections.deque()
#raw=serial.Serial("/dev/ttyACM0",9600)
#raw.open()

def update():
    global data, curve
    #line = raw.readline()
    #data.append(int(line))
    data.append(random.randint(10,15))
    #xdata = np.array(data, dtype='float64')
    curve.setData(data)
    #app.processEvents()

'''
timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(1000)
'''
init = time.time()
while True:

    if time.time() - init <= 1:
        pass
    else:
        update()
        pg.QtGui.QApplication.processEvents()
        init = time.time()

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
