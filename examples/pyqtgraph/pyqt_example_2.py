#import pyqtgraph.examples
#pyqtgraph.examples.run()
import pyqtgraph as pg
import collections, random, time

xVals = collections.deque()
yVals = collections.deque()

try:
	while True:
		for i in range(100):
		  xVals.append(int(i))
		  yVals.append(int(random.randint(10, 100)))
	except (KeyboardInterrupt):
		import sys
		sys.exit(2)

pw = pg.plot(xVals, yVals, pen='g')  # plot x vs y in red
input( )
pw.plot(xVals, yVals, pen='g')
