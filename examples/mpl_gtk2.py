import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from matplotlib.figure import Figure
from numpy import arange, sin, pi
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas

win = Gtk.Window(title="Embedding in GTK")
win.connect("delete-event", Gtk.main_quit)
win.set_default_size(450, 350)

box0 = Gtk.Box(spacing=5)
win.add(box0)

def button_clicked(widget, data):
    if data == 0:
        print('button0 pressed', data)
    elif data == 1:
        print('button1 pressed', data)
    else:
        print('error')


button0 = Gtk.Button(label='button 0')
button0.connect('clicked', button_clicked, 0)
box0.pack_start(button0, True, True, 0)        
        
button1 = Gtk.Button(label='button 1')
button1.connect('clicked', button_clicked, 1)
box0.pack_start(button1, True, True, 0)        
        

f = Figure(figsize=(5, 4), dpi=100)
a = f.add_subplot(111)
t = arange(0.0, 3.0, 0.01)
s = sin(2*pi*t)
a.plot(t, s)

'''
sw = Gtk.ScrolledWindow()
win.add(sw)
# A scrolled window border goes outside the scrollbars and viewport
sw.set_border_width(10)
'''

canvas = FigureCanvas(f)  # a Gtk.DrawingArea
canvas.set_size_request(400, 300)
box0.pack_start(canvas, True, True, 0)        

win.show_all()
Gtk.main()
