import tkinter as tk
import sys
import threading
import time
import collections
import random
import serial

from matplotlib import pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg



class Application(tk.Frame):
	
	def __init__(self, master=None, *args, **kwargs):
		tk.Frame.__init__(self, master, *args, **kwargs)
		self.grid(column=0,row=0)
		self.create_widgets()
		self.data_thread = threading.Thread(target=self.update_data, 
												daemon=True)
												
		self.connect_arduino('/dev/ttyACM0', 115200)
		self.data = 1
		self.serial_input_string = ''
		self.time_stream = collections.deque()
		self.hx711_stream = collections.deque()
		self.hx711_factor = 1
		self.hx711_offset = -8587900.00
		self.temp0_stream = collections.deque()
		self.temp1_stream = collections.deque()


	def connect_arduino(self, dev_handle='/dev/ttyACM0', baudrate=115200):  
		try: 
			print('Starting connection to Arduino. Please wait 2 seconds...')
			self.arduino = serial.Serial(dev_handle, baudrate)
			#print(self.arduino)
			time.sleep(1.5)
			self.arduino.write(b'1')
		except (serial.SerialException):
			print('Arduino not connected! Exiting...')
			sys.exit(1)

	def create_widgets(self):
		self.fig = plt.figure(figsize=(10, 4.5), dpi=100)

		self.ax = self.fig.add_subplot(1,1,1)
		self.ax.set_ylim(0, 100)
		self.line, = self.ax.plot(xar, yar)

		self.canvas = FigureCanvasTkAgg(self.fig, master=root)
		self.canvas.show()
		self.canvas.get_tk_widget().grid(row=0, column=0)
		self.grid(row=1, column=0)

		self.btn0 = tk.Button(self, text="Start data thread", 
									command=self.btn0_f0)
		self.btn0.grid(row=1, column=1)
		
		
		self.btn1 = tk.Button(self, text="Get data", 
									command=self.btn1_f1)
		self.btn1.grid(row=1, column=2)


		self.QUIT = tk.Button(self, text='QUIT', command=root.destroy)
		self.QUIT.grid(row=1, column=3)


	def animate(self,i):
		yar.append(self.hx711_stream)
		xar.append(i)
		self.line.set_data(xar, yar)
		self.ax.set_xlim(0, i+1)


	def get_data():
		self.serial_input_string = str(self.arduino.readline())
		self.serial_input_string = self.serial_input_string.lstrip('b\'')
		self.serial_input_string = self.serial_input_string.rstrip('\\r\\n\'')
		self.serial_input_list = self.serial_input_string.split(' ')
		try:
			# DEBUG:
			#print('RAW:',serial_input_list)
			self.hx711_value = abs(float(self.serial_input_list[0]) * 
								self.hx711_factor + self.hx711_offset)
			if self.serial_input_list[3][1] == '1':	#[1] magnetic switch
				self.time_stream.append(time.time())
				self.hx711_stream.append(hx711_value)
				if self.serial_input_list[1] != 'nan':
					self.temp0_stream.append(float(self.serial_input_list[1]))
				else:
					self.temp0_stream.append(None)
				if self.serial_input_list[2] != 'nan':
					self.temp1_stream.append(float(self.serial_input_list[1]))
				else:
					self.temp1_stream.append(None)
				
				# DEBUG:
				print(self.time_stream[-1], self.hx711_stream[-1], self.temp0_stream[-1], self.temp1_stream[-1])
				
			else:
				pass
		except (ValueError):
			pass
		except (IndexError):
			pass


	def update_data(self):
		while True:
			self.data += 1
			time.sleep(0.01)

	def btn0_f0(self):
		print('Starting data thread')
		self.data_thread.start()

	def btn1_f1(self):
		print(self.data)


if __name__ == "__main__":
	xar = collections.deque()
	yar = collections.deque()
	init_time = time.time()
	
	root = tk.Tk()
	app = Application(master=root)
	ani = animation.FuncAnimation(app.fig, app.animate, interval=500)
		

	#print(app.config())
	root.mainloop()
