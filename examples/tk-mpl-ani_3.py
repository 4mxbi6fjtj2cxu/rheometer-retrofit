from tkinter import *
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
#import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib import style
import collections
import sys
import time
import threading
import serial

style.use(['ggplot', 'fast'])
# Create a thread that always reads arduino and saves to a var and a file

class Application(Frame):
	"""A GUI app with some buttons."""

	def __init__(self, master):
		""" Initialize frame"""
		Frame.__init__(self, master)
		self.grid()
		self.button_clicks=0 #count the number of button click
		self.create_widgets()
		self.data_thread = threading.Thread(target=self.get_data, 
												daemon=True)
		self.graph_thread = threading.Thread(target=self.animate, 
												daemon=True)
		self.enable_data = False
		self.lock = threading.Lock()
												
		self.connect_arduino('/dev/ttyUSB0', 115200)
		self.data = 1
		self.serial_input_string = ''
		self.time_stream = collections.deque()
		self.hx711_stream = collections.deque()
		self.hx711_factor = 1
		self.hx711_offset = 0 # -8587900.00
		self.temp0_stream = collections.deque()
		self.temp1_stream = collections.deque()
		self.counter = 0

	def connect_arduino(self, dev_handle='/dev/ttyUSB0', baudrate=115200):  
		try: 
			print('Starting connection to Arduino. Please wait 2 seconds...')
			self.arduino = serial.Serial(dev_handle, baudrate)
			#print(self.arduino)
			time.sleep(1)
		except (serial.SerialException):
			print('Arduino not connected! Exiting...')
			sys.exit(1)

	def create_widgets(self):
		"""Create button which displays number of clicks."""
        #Button1
		self.button1 = Button(self, text='graph', command=self.update_news)
		self.button1.grid()

		self.QUIT = Button(self, text='exit', command=root.destroy)
		self.QUIT.grid()


	def update_news(self):
		toplevel = Toplevel()
		self.enable_data = True
		self.data_thread.start()

		self.f = Figure(figsize = (5,5), dpi = 100)
		self.a = self.f.add_subplot(111)

		self.canvas = FigureCanvasTkAgg(self.f,toplevel)
		self.canvas.show()
		self.canvas.get_tk_widget().pack()

		toolbar = NavigationToolbar2TkAgg(self.canvas,toplevel)
		toolbar.update()

		self.arduino.write(b'1')

		self.canvas._tkcanvas.pack()
		#self.graph_thread.start()
		#self.ani = animation.FuncAnimation(app.f, app.animate, interval=250)

	def get_data(self):
		while self.enable_data:
			self.serial_input_string = str(self.arduino.readline())
			self.serial_input_string = self.serial_input_string.lstrip('b\'')
			self.serial_input_string = self.serial_input_string.rstrip('\\r\\n\'')
			self.serial_input_list = self.serial_input_string.split(' ')
			try:
				# DEBUG:
				#print('RAW:',serial_input_list)
				if self.serial_input_list[3][1] == '1':	#[1] magnetic switch
					self.hx711_value = abs(float(self.serial_input_list[0]) * 
										self.hx711_factor + self.hx711_offset)
					self.lock.acquire()
					self.time_stream.append(time.time())
					self.hx711_stream.append(self.hx711_value)
					self.counter += 1
					self.lock.release()

					if self.counter == 20:
						self.counter = 0
						self.animate()

					if self.serial_input_list[1] != 'nan':
						self.temp0_stream.append(float(self.serial_input_list[1]))
					else:
						self.temp0_stream.append(None)
					if self.serial_input_list[2] != 'nan':
						self.temp1_stream.append(float(self.serial_input_list[1]))
					else:
						self.temp1_stream.append(None)			
					
					# DEBUG:
					#print(self.time_stream[-1], self.hx711_stream[-1], self.temp0_stream[-1], self.temp1_stream[-1])	
				else:
					pass
			except (ValueError):
				pass
			except (IndexError):
				pass


	def animate(self):
		if (len(self.time_stream) - len(self.hx711_stream)) == 0:
			self.a.clear()
			self.a.plot(self.time_stream, self.hx711_stream)
		else:
			if (len(self.time_stream) - len(self.hx711_stream)) == 1:
				self.hx711_stream.append(self.hx711_stream[-1])
				print('correcting')
			if (len(self.hx711_stream) - len(self.time_stream)) == 1:
				self.time_stream.append(self.time_stream[-1] + 0.01)
				print('correcting')
			else:
				print('Valió mil kilos de verga...')
				print('hx711: ' + len(self.hx711_stream) + ' time: ' + len(self.time_stream))
				sys.exit(1)

#Building the window
root = Tk()
root.title("Buttons")
root.geometry("200x100")

app = Application(root)

#MainLoop
root.mainloop()
