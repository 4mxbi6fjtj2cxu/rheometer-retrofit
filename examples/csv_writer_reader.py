import random, time, csv


def csv_writer(filename):
  with open(filename, 'a', newline='') as csvfile:
    f_writer = csv.writer(csvfile, delimiter=',', quotechar='|')
    for i in range(10):
      f_writer.writerow([time.time(), random.randint(0,1000)])

def csv_reader(filename):
  with open(filename, newline='') as csvfile:
    f_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in f_reader:
      for item in row:
        print(item)      
	#print(','.join(row))
      #print(row)


if __name__ == '__main__':
  csv_writer('file.dat')
  #csv_reader('file.dat')
