#import pyqtgraph.examples
#pyqtgraph.examples.run()
from pyqtgraph.Qt import QtCore, QtGui
import m_average as ma
import pyqtgraph as pg
import numpy as np
import collections, random, csv, time, sys

xVals = collections.deque()
yVals = collections.deque()
yVals2 = collections.deque()

win = pg.GraphicsWindow()
win.setWindowTitle('pyqtgraph example: Scrolling Plots')

p3 = win.addPlot()
win.nextRow()
p4 = win.addPlot()

p3.setDownsampling(mode='peak')
p4.setDownsampling(mode='peak')
p3.setClipToView(True)
p4.setClipToView(True)

now = time.time()
xVals.append(time.time() - now)
yVals.append(random.randint(100, 10000))
yVals2.append(0)

p3.plot(xVals, yVals, pen='g')    # plot x vs y in green
p4.plot(xVals, yVals2, pen='r')    # plot x vs y2 in red



def update():
    xVals.append(time.time() - now)
    yVals.append(random.randint(10000))
    yVals2 = ma.moving_average(yVals, 100, 'simple')

if sys.version_info[0] < 3:
    print("Python 3 or a more recent version is required. Exiting")
    exit(1)
if (sys.flags.interactive != 1) or not hasattr(QtCore,  'PYQT_VERSION'):
    QtGui.QApplication.instance().exec_()

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(100)
