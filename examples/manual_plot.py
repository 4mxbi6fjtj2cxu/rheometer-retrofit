import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
#import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib import style
import collections
import sys
import time


class Sidoso():
	def __init__(self):
		self.time_stream = collections.deque()
		self.hx711_stream = collections.deque()
		self.counter = 0
		self.hl, = plt.plot([], [])


	def update_data(self):
		self.time_stream.append(time.time())
		self.counter += 1
		self.hx711_stream.append(self.counter)


	def graph_data(self):
		self.hl.set_xdata(hl.get_xdata(), self.time_stream)
		self.hl.set_ydata(hl.get_ydata(), self.hx711_stream)
		plt.draw()
		
if __name__ == '__main__':
	win_0 = Sidoso()
	for i in range(10):
		win_0.update_data()
	print(win_0.time_stream)
	print(win_0.hx711_stream)
	win_0.graph_data()
