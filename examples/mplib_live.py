import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
#matplotlib.use('GTKAgg')
import time
import threading
import random
import collections

#print(style.available)
style.use(['ggplot', 'fast'])

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def animate(i):
	graph_data = open('file.dat', 'r').read()
	lines = graph_data.split('\n')
	xs = collections.deque()
	ys = collections.deque()
	for line in lines:
		if len(line) > 1:
			x,y = line.split(',')
			xs.append(x)
			ys.append(y)
	ax1.clear()	
	ax1.plot(xs, ys)

ani = animation.FuncAnimation(fig, animate, interval=1000) #blit=True)
plt.show()



#if __name__ == '__main__':
#    thread = threading.Thread(target=data_listener)
#    thread.daemon = True
#    thread.start()


