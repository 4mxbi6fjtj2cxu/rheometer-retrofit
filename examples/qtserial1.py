from PyQt5 import QtCore
from PyQt5.Qt import *
from PyQt5.QtWidgets import *
from PyQt5.QtSerialPort import *

class Widget(QWidget):
    def __init__(self, parent=None):
        super(Widget, self).__init__(parent)
        self.message_le = QLineEdit()
        self.send_btn = QPushButton(text="Send", clicked=self.send)
        self.output_te = QTextEdit(readOnly=True)
        self.scroll_bar = QScrollBar(self)
        self.output_te.addScrollBarWidget(self.scroll_bar, Qt.AlignLeft)
        self.button = QPushButton(text="Connect", checkable=True, toggled=self.on_toggled)
        lay = QVBoxLayout(self)
        hlay = QHBoxLayout()
        hlay.addWidget(self.message_le)
        hlay.addWidget(self.send_btn)
        lay.addLayout(hlay)
        lay.addWidget(self.output_te)
        lay.addWidget(self.button)

        self.serial = QSerialPort(
            '/dev/ttyUSB0',
            baudRate=QSerialPort.Baud115200,
            readyRead=self.receive )

    @QtCore.pyqtSlot()
    def receive(self):
        while self.serial.canReadLine():
            raw = self.serial.readLine()
            print(raw)
            text = raw.data().decode()
            text = text.rstrip('\r\n')
            self.output_te.append(text)

    @QtCore.pyqtSlot()
    def send(self):
        self.serial.write(self.message_le.text().encode())

    @QtCore.pyqtSlot(bool)
    def on_toggled(self, checked):
        self.button.setText("Disconnect" if checked else "Connect")
        if checked:
            if not self.serial.isOpen():
                if not self.serial.open(QtCore.QIODevice.ReadWrite):
                    self.button.setChecked(False)
        else:
            self.serial.close()

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    w = Widget()
    w.show()
    sys.exit(app.exec_())
