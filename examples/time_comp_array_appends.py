import timeit


s = '''\
import collections
i = collections.deque()
for x in range(500000):
    i.append(time.time())
'''

print(timeit.timeit(stmt=s, number=1))

s = '''\
i = []
for x in range(500000):
    i.append(time.time())
'''

print(timeit.timeit(stmt=s, number=1))

s = '''\
import numpy as np
i = np.array([],float)
for x in range(500000):
    i = np.append(i, time.time())
'''

print(timeit.timeit(stmt=s, number=1))
