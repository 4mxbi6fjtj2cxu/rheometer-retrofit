import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

mywin = Gtk.Window()
mywin.connect('delete-event', Gtk.main_quit)
mywin.show_all()

youwin = Gtk.Window()
youwin.connect('delete-event', Gtk.main_quit)
youwin.show_all()

Gtk.main()
