#!/usr/bin/python3

import sys
from PyQt5.QtWidgets import *       #Basic Qt Imports
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Tu mamá me ama')
        self.setGeometry(10,10,300,200)

        button0 = QPushButton('Button0', self)
        button0.setToolTip('Que le valga verga pariente')
        button0.move(80, 70)
        button0.clicked.connect(self.on_click0)
        self.statusBar().showMessage('Message in statusbar.')
        self.show()

    @pyqtSlot()
    def on_click0(self):
        print('Chingue a su madre viejon')
        self.hide()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
