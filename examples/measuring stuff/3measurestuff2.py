#! /usr/bin/python

import serial
import numpy
from pylab import *
import time

ser = serial.Serial('/dev/ttyACM0', 9600)
time.sleep(1.5)
i=0;
ser.write('a')
a=[]  # create a list
while (i<256):
  a.append(ser.readline())  # append to the list
  print(a[i])
  i=i+1

b=numpy.array(a,'f') # change list to integer array
t = range(256)
plot(t, b, linewidth=1.0)

xlabel('time (s)')
ylabel('voltage (mV)')
title('Simple Arduino Graphing Example')
grid(True)
show()

