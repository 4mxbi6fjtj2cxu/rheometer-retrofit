from matplotlib import pyplot as plt
import math, numpy as np
x = np.arange(0, math.pi*2, 0.05)
y = np.sin(x)
y2 = np.cos(x)
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.legend(   )
ax.set_title("sine wave")
ax.set_xlabel('angle')
ax.set_ylabel('sine')
ax.plot(x,y)
plt.show()

import matplotlib.pyplot as plt
y = [1, 4, 9, 16, 25,36,49, 64]
x1 = [1, 16, 30, 42,55, 68, 77,88]
x2 = [1,6,12,18,28, 40, 52, 65]
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
l1 = ax.plot(x1,y,'ys-') # solid line with yellow colour and square marker
l2 = ax.plot(x2,y,'go--') # dash line with green colour and circle marker
ax.legend(labels = ('tv', 'Smartphone'), loc = 'lower right') # legend placed at lower right
ax.set_title("Advertisement effect on sales")
ax.set_xlabel('medium')
ax.set_ylabel('sales')
plt.show()


from matplotlib import pyplot as plt
import math, numpy as np
x = np.arange(0, math.pi*2, 0.05)
y = np.sin(x)

plt.title('sine wave')
plt.xlabel("angle")
plt.ylabel("sine")
plt.plot(x,y)
plt.show()

