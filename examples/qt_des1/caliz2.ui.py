# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'caliz1.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(662, 479)
        self.gridLayoutWidget = QtGui.QWidget(Form)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 621, 461))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 2, 1, 1)
        self.pushButton0 = QtGui.QPushButton(self.gridLayoutWidget)
        self.pushButton0.setAutoDefault(True)
        self.pushButton0.setFlat(True)
        self.pushButton0.setObjectName(_fromUtf8("pushButton0"))
        self.gridLayout.addWidget(self.pushButton0, 0, 0, 1, 1)
        self.pushButton1 = QtGui.QPushButton(self.gridLayoutWidget)
        self.pushButton1.setAutoDefault(False)
        self.pushButton1.setDefault(False)
        self.pushButton1.setFlat(True)
        self.pushButton1.setObjectName(_fromUtf8("pushButton1"))
        self.gridLayout.addWidget(self.pushButton1, 1, 0, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 2, 0, 1, 1)
        self.graphicsView = PlotWidget(self.gridLayoutWidget)
        self.graphicsView.setEnabled(True)
        self.graphicsView.setObjectName(_fromUtf8("graphicsView"))
        self.gridLayout.addWidget(self.graphicsView, 0, 1, 3, 2)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Me la pelan turistas", None))
        self.pushButton0.setText(_translate("Form", "Sidral", None))
        self.pushButton1.setText(_translate("Form", "Nelson", None))

from pyqtgraph import PlotWidget

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

