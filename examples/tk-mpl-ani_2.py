import tkinter as Tk
from tkinter import ttk
import matplotlib
from matplotlib import style
from matplotlib.figure import Figure
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
matplotlib.use('TkAgg')
import serial
import sys
import time

style.use(['ggplot', 'fast'])

time_stream = []
hx711_stream = []
hx711_factor = 1
hx711_offset = -8587900.00
temp0_stream = []
temp1_stream = []

f = Figure(figsize=(5,5), dpi=100)
a = f.add_subplot(111)


try: 
	arduino = serial.Serial('/dev/ttyACM0', 115200)
	print('Starting connection to arduino. Please wait...')
	time.sleep(1.5)
	arduino.write(b'1')
except (serial.SerialException):
	print('Arduino not connected!')
	sys.exit(1)




def get_data():
	serial_input_string = str(arduino.readline())
	serial_input_string = serial_input_string.lstrip('b\'')
	serial_input_string = serial_input_string.rstrip('\\r\\n\'')
	serial_input_list = serial_input_string.split(' ')
	try:
		# DEBUG:
		#print('RAW:',serial_input_list)
		hx711_value = abs(float(serial_input_list[0]) * 
							hx711_factor + hx711_offset)
		if serial_input_list[3][1] == '1':	#[1] magnetic switch
			time_stream.append(time.time())
			hx711_stream.append(hx711_value)
			if serial_input_list[1] != 'nan':
				temp0_stream.append(float(serial_input_list[1]))
			else:
				temp0_stream.append(None)
			if serial_input_list[2] != 'nan':
				temp1_stream.append(float(serial_input_list[1]))
			else:
				temp1_stream.append(None)
			
			# DEBUG:
			#print(time_stream[-1], hx711_stream[-1], temp0_stream[-1], temp1_stream[-1])
			
		else:
			pass
	except (ValueError):
		pass
	except (IndexError):
		pass
	
	a.clear()
	a.plot(time_stream, hx711_stream)

def animate(i):
	serial_input_string = str(arduino.readline())
	serial_input_string = serial_input_string.lstrip('b\'')
	serial_input_string = serial_input_string.rstrip('\\r\\n\'')
	serial_input_list = serial_input_string.split(' ')
	# DEBUG:
	#print('RAW:',serial_input_list)
	hx711_value = abs(float(serial_input_list[0]) * 
							hx711_factor + hx711_offset)
	time_stream.append(time.time())
	hx711_stream.append(hx711_value)
	self.line.set_data(xar, yar)

class Visual_reometer(Tk.Tk):
	def __init__(self, *args, **kwargs):
		Tk.Tk.__init__(self, *args, **kwargs)
		Tk.Tk.wm_title(self, 'Reometro Visualizacion')
		container = Tk.Frame(self)


app = Visual_reometer()
ani = animation.FuncAnimation(f, animate, interval=100)
